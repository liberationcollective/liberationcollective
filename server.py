from flask import Flask, render_template, request
from os import path
from flask.ext.zodb import ZODB
app = Flask(__name__)
app.config['ZODB_STORAGE'] = 'file://data/app.fs'
db = ZODB(app)

@app.route("/", methods=['GET', 'POST'])
def index():
    submitted = request.method == 'POST'
    email = None
    if submitted:
        email = request.form['email']
        db.setdefault('emails', [])
        db['emails'].append(email)
    return render_template('index.html', submitted=submitted, email=email)

if __name__ == "__main__":
    app.run(
        host="0.0.0.0",
        port=5000
    )
